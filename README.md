# FlickrBoi

Purpose:
Get images from flickr api and display them in a thumbnail gallery with a main image that can be
swiped to go forward / backwards through the images. There is also a refresh button to refresh the
images.

** App SDK versions **
minSdkVersion 21
targetSdkVersion 27

** Gradle Version **
3.1.2

** Kotlin Extension Version **
kotlin_version = '1.2.40'

Built using Android Studio version 3.1.2

Currently supports two screen sizes: phone and tablet (600dp smallest screen width)

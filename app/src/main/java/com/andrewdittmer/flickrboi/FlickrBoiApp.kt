package com.andrewdittmer.flickrboi

import android.app.Application
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.ProcessLifecycleOwner
import com.andrewdittmer.flickrboi.data.NetworkModule
import com.andrewdittmer.flickrboi.injection.AppComponent
import com.andrewdittmer.flickrboi.injection.AppModule
import com.andrewdittmer.flickrboi.injection.DaggerAppComponent


class FlickrBoiApp : Application(), LifecycleObserver {

    companion object {
        lateinit var appComponent: AppComponent
    }


    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule("https://api.flickr.com"))
            .build()

        appComponent.inject(this)
    }

}
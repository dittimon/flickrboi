package com.andrewdittmer.flickrboi.data

import com.andrewdittmer.flickrboi.model.FlickrPhotosResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface FlickrApi {
    @GET("/services/feeds/photos_public.gne?format=json&nojsoncallback=1")
    fun getFlickrPosts() : Observable<FlickrPhotosResponse>
}
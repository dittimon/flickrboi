package com.andrewdittmer.flickrboi.data

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.andrewdittmer.flickrboi.model.FlickrPhotosResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DataManager @Inject constructor(private val flickrApi: FlickrApi) {

    private val allCompositeDisposable: MutableList<Disposable> = arrayListOf()

    fun getFlickrPhotos() : LiveData<FlickrPhotosResponse> {
        val mutableLiveData = MutableLiveData<FlickrPhotosResponse>()
        val disposable = flickrApi.getFlickrPosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                result -> mutableLiveData.value = result
            }, {
                t: Throwable? ->
                mutableLiveData.value = FlickrPhotosResponse(ArrayList())
                Log.e("DataManager", t?.message)
            })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }
}
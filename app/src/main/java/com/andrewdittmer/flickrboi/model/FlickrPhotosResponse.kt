package com.andrewdittmer.flickrboi.model

data class FlickrPhotosResponse(val items: List<FlickrItem>)
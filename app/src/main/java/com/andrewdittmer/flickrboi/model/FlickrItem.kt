package com.andrewdittmer.flickrboi.model

data class FlickrItem(val media: Media) {
    data class Media(val m: String)
}
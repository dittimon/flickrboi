package com.andrewdittmer.flickrboi.injection

import com.andrewdittmer.flickrboi.FlickrBoiApp
import com.andrewdittmer.flickrboi.ui.photo.PhotoViewModel
import com.andrewdittmer.flickrboi.data.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, NetworkModule::class])
@Singleton
interface AppComponent {

    fun inject(flickrBoiApp: FlickrBoiApp)
    fun inject(photoViewModel: PhotoViewModel)
}
package com.andrewdittmer.flickrboi.injection

import android.content.Context
import com.andrewdittmer.flickrboi.FlickrBoiApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val flickrBoiApp: FlickrBoiApp) {

    @Provides
    @Singleton
    fun provideContext(): Context = flickrBoiApp

}
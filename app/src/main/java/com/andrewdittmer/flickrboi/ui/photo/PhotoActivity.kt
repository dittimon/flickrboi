package com.andrewdittmer.flickrboi.ui.photo

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.res.Configuration
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSmoothScroller
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import com.andrewdittmer.flickrboi.R
import com.andrewdittmer.flickrboi.R.id.progressBar
import com.andrewdittmer.flickrboi.R.id.thumbnailRecyclerView
import com.andrewdittmer.flickrboi.model.FlickrItem
import com.andrewdittmer.flickrboi.ui.photo.pager.LargePhotoFragment
import com.andrewdittmer.flickrboi.util.NetworkUtil
import kotlinx.android.synthetic.main.activity_photo.*


class PhotoActivity : AppCompatActivity(), PhotoViewHolder.OnItemClickListener {

    private var photoViewModel: PhotoViewModel? = null
    private lateinit var pagerAdapter: PagerAdapter
    private lateinit var flickrItems: List<FlickrItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        initViewModel()

        thumbnailRecyclerView.layoutManager = getScreenAppropriateLayoutManager()


        if (NetworkUtil.isNetworkAvailable(this)) {
            showLoading(true)
            photoViewModel?.getFlickrPhotos()?.observe(this, Observer {

                setAdapter(it!!.items)
                showLoading(false)
            })
        } else {
            showNoInternetMessage()
        }

    }

    private fun setAdapter(items: List<FlickrItem>) {
        flickrItems = items

        if (items.isEmpty()) {
            Snackbar.make(imagePager, R.string.no_items, Snackbar.LENGTH_LONG).show()
        } else {
            val adapter = PhotoAdapter(flickrItems!!)
            adapter.setPhotoClickListener(this)
            thumbnailRecyclerView.adapter = adapter

            pagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)
            imagePager?.adapter = pagerAdapter
            imagePager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }
                override fun onPageSelected(position: Int) {
                    (thumbnailRecyclerView.adapter as PhotoAdapter).setSelectedPosition(position)

                    // If the selected item in the thumbnail list is not visible, we should scroll to it.
                    val smoothScroller = object : LinearSmoothScroller(this@PhotoActivity) {
                        override fun getVerticalSnapPreference(): Int {
                            return LinearSmoothScroller.SNAP_TO_ANY
                        }

                        override fun getHorizontalSnapPreference(): Int {
                            return LinearSmoothScroller.SNAP_TO_ANY
                        }
                    }
                    smoothScroller.targetPosition = position
                    thumbnailRecyclerView.layoutManager.startSmoothScroll(smoothScroller)
                }

            })
        }

    }

    private fun getScreenAppropriateLayoutManager(): RecyclerView.LayoutManager {
        val layoutManager: RecyclerView.LayoutManager
        val useThumbnailGrid = resources.getBoolean(R.bool.use_thumbnail_grid)

        // Large screens will use a grid to display thumbnails
        if (useThumbnailGrid) {
            layoutManager = GridLayoutManager(this@PhotoActivity, 5)
        } else {

            // Small screens in landscape mode will show vertical recyclerview to save vertical space
            if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
                layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            } else {
                layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            }
        }

        return layoutManager
    }

    private fun refresh() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            showLoading(true)
            photoViewModel?.refreshFlickrPhotos()?.observe(this, Observer {
                setAdapter(it!!.items)
                showLoading(false)
            })
        } else {
            showNoInternetMessage()
        }

    }

    private fun showLoading(visible: Boolean) {
        if (visible) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.INVISIBLE
        }
    }

    private fun showNoInternetMessage() {
        Snackbar.make(thumbnailRecyclerView, R.string.no_internet, Snackbar.LENGTH_LONG).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_photo_viewer, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == R.id.refresh) {
            refresh()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initViewModel() {
        photoViewModel = ViewModelProviders.of(this).get(
            PhotoViewModel::class.java)
    }

    override fun onItemClick(index: Int) {
        (thumbnailRecyclerView.adapter as PhotoAdapter).setSelectedPosition(index)
        imagePager?.setCurrentItem(index, false)
    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return LargePhotoFragment.newInstance(flickrItems[position].media.m)
        }

        override fun getCount(): Int {
            return flickrItems.size
        }
    }
}

package com.andrewdittmer.flickrboi.ui.photo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.andrewdittmer.flickrboi.FlickrBoiApp
import com.andrewdittmer.flickrboi.data.DataManager
import com.andrewdittmer.flickrboi.model.FlickrPhotosResponse
import javax.inject.Inject

class PhotoViewModel: ViewModel() {

    @Inject
    lateinit var dataManager: DataManager

    var flickrPhotosResponse: LiveData<FlickrPhotosResponse>? = null

    init {
        initializeDagger()
    }

    private fun initializeDagger() = FlickrBoiApp.appComponent.inject(this)

    fun getFlickrPhotos():LiveData<FlickrPhotosResponse> {

        if (flickrPhotosResponse == null) {
            flickrPhotosResponse = dataManager.getFlickrPhotos()
        }

        return flickrPhotosResponse as LiveData<FlickrPhotosResponse>
    }

    fun refreshFlickrPhotos():LiveData<FlickrPhotosResponse> {

        flickrPhotosResponse = dataManager.getFlickrPhotos()

        return flickrPhotosResponse as LiveData<FlickrPhotosResponse>
    }

}
package com.andrewdittmer.flickrboi.ui.photo

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.andrewdittmer.flickrboi.R
import com.andrewdittmer.flickrboi.model.FlickrItem

class PhotoAdapter(private var items: List<FlickrItem>) : RecyclerView.Adapter<PhotoViewHolder>() {

    private lateinit var listener: PhotoViewHolder.OnItemClickListener
    private var selectedPos = 0

    override fun getItemCount(): Int {
        return items.size
    }

    fun getItem(position: Int) = items[position]

    fun setPhotoClickListener(listener: PhotoViewHolder.OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context).inflate(
            R.layout.view_flickr_item, parent, false)
        return PhotoViewHolder(layoutInflater)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
        holder.itemView.tag = position
        holder.itemView.isSelected = holder.itemView.tag as Int == selectedPos
    }

    fun setSelectedPosition(position: Int) {
        selectedPos = position
        notifyDataSetChanged()
    }

}
package com.andrewdittmer.flickrboi.ui.photo.pager


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.ScaleGestureDetector
import android.view.View
import android.view.ViewGroup
import com.andrewdittmer.flickrboi.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_large_photo.*


private const val ARG_PHOTO_URL = "photoUrl"

/**
 *
 * Displays a large image from url
 *
 */
class LargePhotoFragment : Fragment() {
    private var photoUrl: String? = null

    private var scaleGestureDetector: ScaleGestureDetector? = null
    private var scaleFactor = 1.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            photoUrl = it.getString(ARG_PHOTO_URL)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_large_photo, container, false)
        view.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
            scaleGestureDetector?.onTouchEvent(motionEvent)
            return@OnTouchListener true
        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        scaleGestureDetector = ScaleGestureDetector(context, ScaleListener())

        Glide
            .with(this)
            .load(photoUrl)
            .apply(RequestOptions.centerCropTransform())
            .into(largePhotoImageView)
    }

    private inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {

        override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
            scaleFactor *= scaleGestureDetector.scaleFactor
            scaleFactor = Math.max(MIN_ZOOM, Math.min(scaleFactor, MAX_ZOOM))
            largePhotoImageView.scaleX = scaleFactor
            largePhotoImageView.scaleY = scaleFactor
            return true
        }
    }

    companion object {
        const val MIN_ZOOM = 1.0f
        const val MAX_ZOOM = 5.0f
        /**
         * Create a new instance of LargePhotoFragment
         *
         * @param photoUrl The url of the image to display
         * @return A new instance of fragment LargePhotoFragment.
         */
        @JvmStatic
        fun newInstance(photoUrl: String) =
            LargePhotoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PHOTO_URL, photoUrl)
                }
            }
    }
}

package com.andrewdittmer.flickrboi.ui.photo

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import com.andrewdittmer.flickrboi.R
import com.andrewdittmer.flickrboi.injection.GlideApp
import com.andrewdittmer.flickrboi.model.FlickrItem
import kotlinx.android.synthetic.main.view_flickr_item.view.*
import com.bumptech.glide.request.RequestOptions.centerCropTransform

class PhotoViewHolder(private val root: View) : ViewHolder(root) {
    fun bind(item: FlickrItem, clickListener: OnItemClickListener) {

        GlideApp
            .with(root)
            .load(item.media.m)
            .placeholder(R.drawable.flickr_dots)
            .apply(centerCropTransform())
            .into(root.thumbnailImageView)


        root.setOnClickListener{
            clickListener.onItemClick(root.tag as Int)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(index: Int)
    }
}